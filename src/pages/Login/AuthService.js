import API from "./../../providers/empresas.providers";

export default class AuthService {
  constructor() {}

  // Retorna a requisição de autenticação da API
  signIn(data) {
    return new API().signIn(data);
  }

  // Salva o token no localStorage
  setToken(data) {
    localStorage.setItem("EMPRESAS_access_token", data.access_token);
    localStorage.setItem("EMPRESAS_client", data.client);
    localStorage.setItem("EMPRESAS_uid", data.uid);
  }

  // Retorna o token do localStorage
  getToken() {
    return {
      access_token: localStorage.getItem("EMPRESAS_access_token"),
      client: localStorage.getItem("EMPRESAS_client"),
      uid: localStorage.getItem("EMPRESAS_uid")
    };
  }

  // Verifica se o token existe
  loggedIn() {
    return !!this.getToken().access_token;
  }

  // Deleta o token do localStorage
  logout() {
    localStorage.removeItem("EMPRESAS_access_token");
    localStorage.removeItem("EMPRESAS_client");
    localStorage.removeItem("EMPRESAS_uid");
  }
}
