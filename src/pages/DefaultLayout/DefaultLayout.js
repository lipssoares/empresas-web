import React, { Suspense } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import PropTypes from "prop-types";
import classNames from "classnames";

import { withStyles } from "@material-ui/core/styles";
import withRoot from "../../withRoot";

// import styles from "./DefaultLayout.module.scss";

const styles = theme => ({
  root: {
    // paddingTop: theme.spacing.unit * 20
  }
});

class DefaultLayout extends React.Component {
  render() {
    const { classes, routes } = this.props;

    return (
      <div className={classNames(classes.root)}>
        <BrowserRouter>
          <Suspense fallback={<main className="main">Carregando...</main>}>
            <Switch>
              {routes.map((route, index) => {
                return route.component ? (
                  <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    render={props => <route.component {...props} />}
                  />
                ) : null;
              })}
            </Switch>
          </Suspense>
        </BrowserRouter>
      </div>
    );
  }
}

DefaultLayout.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRoot(withStyles(styles)(DefaultLayout));
