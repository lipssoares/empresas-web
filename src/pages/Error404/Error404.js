import React from "react";
import classNames from "classnames";

import { Typography } from "@material-ui/core";

import styles from "./Error404.module.scss";

class Error404 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <main className={classNames(styles.Error404, "container")}>
        <Typography component="h1" variant="h4" gutterBottom>
          Página não encontrada
        </Typography>
      </main>
    );
  }
}

export default Error404;
