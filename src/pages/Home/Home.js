import React from "react";
import classNames from "classnames";

import Header from "./../../components/Header";
import CardEmpresa from "./../../components/CardEmpresa";

import API from "./../../providers/empresas.providers";

import styles from "./Home.module.scss";

// Função que remove acentos de uma string
const removeAccents = s => {
  return s
    .normalize("NFD")
    .replace(/[\u0300-\u036f|\u00b4|\u0060|\u005e|\u007e]/g, "");
};

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      enterprises: [],
      results: [],
      feedback: "Carregando empresas...",
      visaoPesquisa: false,
      filter: "",
      visaoDetalhe: false
    };
    this.setVisaoPesquisa = this.setVisaoPesquisa.bind(this);
    this.setFilter = this.setFilter.bind(this);
  }

  componentDidMount() {
    this.getEmpresas();
  }

  getEmpresas() {
    new API().getEmpresas().then(
      function(response) {
        let contentType = response.headers.get("Content-Type");

        if (contentType && contentType.indexOf("application/json") !== -1) {
          if (response.ok) {
            return response.json().then(
              function(json) {
                this.setState({
                  enterprises: json.enterprises,
                  results: json.enterprises,
                  feedback: ""
                });
              }.bind(this)
            );
          } else {
            this.setState({ feedback: "Bad request! :(" });
          }
        } else {
          this.setState({ feedback: "Erro ao tentar processar a requisição :(" });
        }
      }.bind(this)
    );
  }

  setVisaoPesquisa = visaoPesquisa => {
    this.setState({ visaoPesquisa: visaoPesquisa });
  };

  // Função auxiliar que filtra o vetor de objetos conforme o termo pesquisado
  filter = (q, arr) => {
    q = removeAccents(q).toLowerCase();

    return arr.filter(
      item =>
        removeAccents(
          item.enterprise_name + item.enterprise_type.enterprise_type_name
        )
          .toLowerCase()
          .indexOf(q) != -1
    );
  };

  setFilter(q) {
    if (q) {
      this.setState({
        filter: q,
        visaoDetalhe: false,
        results: this.filter(q, this.state.enterprises)
      });
    } else {
      this.setState({
        filter: "",
        visaoDetalhe: false,
        results: this.state.enterprises
      });
    }
  }

  render() {
    return (
      <div className={classNames(styles.Home)}>
        <Header
          visaoPesquisa={this.state.visaoPesquisa}
          setVisaoPesquisa={this.setVisaoPesquisa}
          filter={this.state.filter}
          setFilter={this.setFilter}
        />
        <main className={classNames("container", styles.container)}>
          {this.state.visaoPesquisa &&
          this.state.filter &&
          this.state.results &&
          this.state.results.length ? (
            this.state.results.map((item, index) => (
              <CardEmpresa
                enterprise={item}
                visaoDetalhe={this.state.visaoDetalhe}
                index={index}
                handleClick={() => {
                  if (!this.state.visaoDetalhe)
                    this.setState({
                      visaoDetalhe: true,
                      results: this.state.enterprises.filter(
                        x => x.id == item.id
                      )
                    });
                  else this.setFilter(this.state.filter);
                }}
              />
            ))
          ) : (
            <div className={styles.feedback}>
              <span>
                {this.state.visaoPesquisa
                  ? this.state.filter
                    ? `Sua busca por "${
                        this.state.filter
                      }" não retornou resultados.`
                    : this.state.feedback
                  : "Clique na busca para iniciar."}
              </span>
            </div>
          )}
        </main>
      </div>
    );
  }
}

export default Home;
