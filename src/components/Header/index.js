import React, { useState, useEffect } from "react";
import classNames from "classnames";

import { TextField, InputAdornment } from "@material-ui/core";
import { SearchOutlined, ExitToAppOutlined } from "@material-ui/icons";

import styles from "./Header.module.scss";
import logo from "./../../images/logo-nav@3x.png";

import AuthService from "./../../pages/Login/AuthService";

function Header(props) {
  const { visaoPesquisa, setVisaoPesquisa, filter, setFilter } = props;

  return (
    <header className={styles.Header}>
      <div
        className={classNames(
          "container",
          styles.container,
          visaoPesquisa ? styles.visaoPesquisa : ""
        )}
      >
        <div className={styles.startBar}>
          <SearchOutlined
            className={styles.search}
            onClick={() => setVisaoPesquisa(true)}
          />
          <img src={logo} alt="Logo ioasys" className={styles.logo} />
          <ExitToAppOutlined
            className={styles.logout}
            onClick={() => {
              new AuthService().logout();
              window.location = "/login";
            }}
          />
        </div>
        <div className={styles.searchBar}>
          <TextField
            id="search"
            placeholder="Pesquisar"
            value={filter}
            fullWidth={true}
            className={styles.inputField}
            InputProps={{
              startAdornment: (
                <div>
                  <InputAdornment position="start">
                    <SearchOutlined />
                  </InputAdornment>
                  <InputAdornment position="end">
                    {/* {filter ? () : ("")} */}
                    <i
                      className={styles.iconCancel}
                      onClick={() => {
                        // if (!filter)
                        setVisaoPesquisa(false);
                        setFilter("");
                      }}
                    />
                  </InputAdornment>
                </div>
              )
            }}
            onChange={event => {
              setFilter(event.target.value);
            }}
          />
        </div>
      </div>
    </header>
  );
}

export default Header;
