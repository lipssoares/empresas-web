import React, { useState, useEffect } from "react";
import classNames from "classnames";

import { Card } from "@material-ui/core";

import styles from "./CardEmpresa.module.scss";
import mock from "./../../images/mock-empresa.svg";

function CardEmpresa(props) {
  const { enterprise, visaoDetalhe, index, handleClick } = props;

  return (
    <Card
      className={classNames(
        styles.CardEmpresa,
        visaoDetalhe ? styles.visaoDetalhe : ""
      )}
      key={index}
      onClick={handleClick}
    >
      <div className={styles.imagem}>
        <img src={mock} alt={enterprise.enterprise_name} />
      </div>
      <div className={styles.informacoes}>
        <strong className={styles.nome}>{enterprise.enterprise_name}</strong>
        <span className={styles.negocio}>
          {enterprise.enterprise_type.enterprise_type_name}
        </span>
        <span className={styles.localizacao}>{enterprise.country}</span>
      </div>
      <div className={styles.descricao}>
        <p>{enterprise.description}</p>
      </div>
    </Card>
  );
}

export default CardEmpresa;
