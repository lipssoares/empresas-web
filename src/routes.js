import React, { lazy } from "react";

const Home = lazy(() => import("./pages/Home"));
const Error404 = lazy(() => import("./pages/Error404"));

export const routes = [
  { path: "/", exact: true, component: Home },
  // {
  //   path: "/enterprises/:id",
  //   exact: true,
  //   component: Empresa
  // },
  {
    path: "",
    exact: true,
    component: Error404
  }
];
